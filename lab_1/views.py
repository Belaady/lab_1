from django.shortcuts import render,redirect
from datetime import datetime, date
from .forms import *
from .models import *
# Create your views here.
def index(request):
    response = {}
    return render(request, 'index_lab1.html', response)
def profile(request):
    response = {}
    return render(request, 'profile.html', response)
def experiences(request):
    response = {}
    return render(request, 'experiences.html', response)
def compfest(request):
    response = {}
    return render(request, 'compfest.html', response)
def cgt(request):
    response = {}
    return render(request, 'cgt.html', response)
def universiade(request):
    response = {}
    return render(request, 'universiade.html', response)
def betis(request):
    response = {}
    return render(request, 'betis.html', response)
def gallery(request):
    response = {}
    return render(request, 'gallery.html', response)
def addSchedule(request):
    form = Message_Form()
    response = {'message_form': form}
    return render(request, 'addSchedule.html', response)
def listSchedule(request):
    schedules = Schedule.objects.all()
    response = {"myList" : schedules}
    return render(request, 'listSchedule.html', response)
def saveschedule(request) :
    if request.method == 'POST':
        activity = request.POST['activity']
        place = request.POST['place']
        date = request.POST['date']
        time = request.POST['time']
        description = request.POST['description']
        schedule = Schedule(activity=activity,place=place,date=date,time=time,description=description)
        schedule.save()
        return redirect('listSchedule')
def delete(request, id) :
    if request.method == 'POST':
        selectedSchedule = Schedule.objects.get(pk=id)
        selectedSchedule.delete()
        return redirect('listSchedule')

