from django.urls import path
from .views import index,profile, experiences, compfest, cgt, universiade, betis, gallery, addSchedule, listSchedule, saveschedule, delete
#url for app
urlpatterns = [
    path('', index, name='index'),
    path('profile', profile, name='profile'),
    path('experiences', experiences, name='experiences'),
    path('compfest', compfest, name='compfest'),
    path('cgt', cgt, name='cgt'),
    path('universiade', universiade, name='universiade'),
    path('betis', betis, name='betis'),
    path('gallery', gallery, name='gallery'),
    path('addSchedule', addSchedule, name='addSchedule'),
    path('listSchedule', listSchedule, name='listSchedule'),
    path('saveschedule', saveschedule, name= 'saveschedule'),
    path('delete/<int:id>', delete, name='delete')


]
