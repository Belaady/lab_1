from django import forms
import datetime
class Message_Form(forms.Form):
        activity = forms.CharField(label='activity', required=True, 
                max_length=90, 
                widget=forms.TextInput(attrs = {"class": "form-control"}))
        place = forms.CharField(required=True, 
                widget=forms.TextInput(attrs = {"class": "form-control"}))
        date = forms.DateField(initial=datetime.date.today, 
                required=True, widget=forms.DateTimeInput(attrs = {"class": "form-control", "type" : "date"}))
        time = forms.TimeField(initial=datetime.date.today, 
                required=True, widget=forms.DateTimeInput(attrs = {"class": "form-control", "type" : "time"}))
        description = forms.CharField(required=True, 
                widget=forms.Textarea(attrs = {"class": "form-control"}))
